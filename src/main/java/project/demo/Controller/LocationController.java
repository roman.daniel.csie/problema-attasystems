package project.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.demo.Model.Location;
import project.demo.ObjHolder;
import project.demo.Service.LocationService;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/locations")
public class LocationController {
    @Autowired
    LocationService locationService;

    @GetMapping("/list-all")
    public List<Location> list() {
        return locationService.listAllLocations();
    }

    @GetMapping("/list-all-place")
    public List<Location> listLocationsByPlace(@RequestBody String place) {
        return locationService.listAllLocationsByPlace(place);
    }

    @GetMapping("/list-all-sport")
    public List<Location> listLocationsBySport(@RequestBody String sport) {
        return locationService.listAllLocationsBySport(sport);
    }

    @GetMapping("/list-all-period")
    public List<Location> listLocationsByPeriod(@RequestBody ObjHolder objHolder) {
        return locationService.listAllLocationsByPeriod(objHolder.getString1(), objHolder.getString2());
    }

    @GetMapping("/list-all-place-sport")
    public List<Location> listLocationsByPlaceAndSport(@RequestBody ObjHolder objHolder) {
        return locationService.listAllLocationsByPlaceAndSport(objHolder.getString1(), objHolder.getString2());
    }



    @GetMapping("/{id}")
    public ResponseEntity<Location> get(@PathVariable Integer id) {
        try {
            Location location = locationService.getLocation(id);
            return new ResponseEntity<Location>(location, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Location>(HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping("/addLocation")
    public ResponseEntity<?> add(@RequestBody Location location) {
        locationService.saveLocation(location);
        return new ResponseEntity<>(location, HttpStatus.OK);

    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updatePrice(@RequestBody float newAverageCost, @PathVariable Integer id) {
        try {
            Location existentLocation = locationService.getLocation(id);
            existentLocation.setAverage_cost(newAverageCost);
            locationService.saveLocation(existentLocation);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        locationService.deleteLocationById(id);
    }

}

