package project.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import project.demo.Model.Location;

import java.util.List;

public interface LocationRepository extends JpaRepository<Location, Integer> {

    @Query(value = "select * from locations where country = :place or region = :place or city = :place order by average_cost",nativeQuery = true)
    List<Location> findByPlace(@Param("place") String place);


    @Query(value = "select * from locations where practiced_sport = :sport order by average_cost",nativeQuery = true)
    List<Location> findBySport(@Param("sport") String sport);

    @Query(value = "select * from locations where :trip_starting_date between season_starting_date and season_ending_date and :trip_ending_date between season_starting_date and season_ending_date order by average_cost",nativeQuery = true)
    List<Location> findByPeriod(@Param("trip_starting_date") String trip_starting_date,@Param("trip_ending_date") String trip_ending_date);

    @Query(value = "select * from locations where country = :place or region = :place or city = :place and practiced_sport = :sport order by average_cost",nativeQuery = true)
    List<Location> findByPlaceAndSport(@Param("place") String place,@Param("sport") String sport);


}
