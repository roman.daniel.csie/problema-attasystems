package project.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import project.demo.Model.Location;
import project.demo.Repository.LocationRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LocationService {
    @Autowired
    private LocationRepository locationRepository;
    public List<Location> listAllLocations() {
        return locationRepository.findAll();
    }

    public List<Location> listAllLocationsByPlace(String place) {
        return locationRepository.findByPlace(place);
    }

    public List<Location> listAllLocationsBySport(String sport) {
        return locationRepository.findBySport(sport);
    }

    public List<Location> listAllLocationsByPeriod(String trip_starting_date, String trip_ending_date) {
        return locationRepository.findByPeriod(trip_starting_date,trip_ending_date);
    }

    public List<Location> listAllLocationsByPlaceAndSport(String place, String sport) {
        return locationRepository.findByPlaceAndSport(place,sport);
    }

    public void saveLocation(Location location) {
        locationRepository.save(location);
    }

    public Location getLocation(Integer id) {
        return locationRepository.findById(id).get();
    }

    public void deleteLocationById(Integer id) {
        locationRepository.deleteById(id);
    }



}
