package project.demo.Model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "locations")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String practiced_sport;
    private String name;
    private String country;
    private String region;
    private String city;
    private float average_cost;
    private Timestamp season_starting_date;
    private Timestamp season_ending_date;


    public Location() {
    }

    public Location(String practiced_sport, String name, String country, String region, String city, float average_cost, Timestamp season_starting_date, Timestamp season_ending_date) {
        this.practiced_sport = practiced_sport;
        this.name = name;
        this.country = country;
        this.region = region;
        this.city = city;
        this.average_cost = average_cost;
        this.season_starting_date = season_starting_date;
        this.season_ending_date = season_ending_date;
    }

    public String getPracticed_sport() {
        return practiced_sport;
    }

    public void setPracticed_sport(String practiced_sport) {
        this.practiced_sport = practiced_sport;
    }

    public Timestamp getSeason_starting_date() {
        return season_starting_date;
    }

    public void setSeason_starting_date(Timestamp season_starting_date) {
        this.season_starting_date = season_starting_date;
    }

    public Timestamp getSeason_ending_date() {
        return season_ending_date;
    }

    public void setSeason_ending_date(Timestamp season_ending_date) {
        this.season_ending_date = season_ending_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getAverage_cost() {
        return average_cost;
    }

    public void setAverage_cost(float average_cost) {
        this.average_cost = average_cost;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", practicedSport='" + practiced_sport + '\'' +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", average_cost=" + average_cost +
                ", seasonStartingDate=" + season_starting_date +
                ", seasonEndingDate=" + season_ending_date +
                '}';
    }
}


